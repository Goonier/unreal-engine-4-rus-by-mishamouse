# Making Interactive Experiences
How to create gameplay mechanics, behaviors, and conditions that make the virtual world responsive to players carrying out actions over time.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/InteractiveExperiences_TopicImage.webp" width="1000">

This section contains information about high-level gameplay programming and scripting in Unreal Engine, with an aim towards facilitating interaction between the player and the world. In addition to the basic Gameplay Framework, Unreal Engine includes many systems and frameworks for handling common gameplay elements, including AI, physics, user interfaces, camera management, and input. The guides in this section will provide a reference for how to use these features, as well as walkthroughs for how to re-create common mechanics and systems in games. 

## Index

##### Gameplay Framework
Core systems, such as game rules, player input and controls, cameras, and user interfaces.

##### Gameplay How-To Guides
Walkthroughs for re-creating common gameplay elements

##### Vehicles
Vehicles

##### Artificial Intelligence
Describes the systems available within Unreal Engine 4 that can be used to create believable AI entities in your projects.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/ArtificialIntelligence/AI_Landing_Page_Image.webp" width="460">

##### Using Dialogue Voices and Waves
Example of a dialogue setup with different listeners and dialogue contexts

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Dialogue/audio_topic.webp" width="460">

##### Physics
Subsystem that calculates collision and simulate physical actors.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Physics/physics_topic.webp" width="460">

##### UMG UI Designer
A guide to using Unreal Motion Graphics to create UI elements.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/UMG/UMG_topic.webp" width="460">

##### Gameplay Ability System
High-level view of the Gameplay Ability System

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/GameplayAbilitySystem/GameplayAbilitySystemTopicImage_01.webp" width="460">

##### Input
The Input object is responsible for converting input from the player into data in a form Actors can understand and make use of.

##### Saving and Loading Your Game
Overview of how to save and load your game

##### Using Cameras
A How To Guide for Finding Actors in your Scenes in Unreal Engine 4.

##### Traces with Raycasts
The landing page for Physics Traces.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Tracing/trace_topic.webp" width="460">

##### Data Driven Gameplay Elements
Driving gameplay elements using externally stored data.

##### Force Feedback
Using the vibration functionality of mobile devices and controllers to convey a force occuring in the game to the player.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/ForceFeedback/force_feedback_topic.webp" width="460">

##### Using Timers
A How To Guide for Using Timers for Gameplay in Unreal Engine 4.

##### Networking and Multiplayer
Setting up networked games for multiplayer.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Networking/NetworkingAndMultiplayer_Topic.webp" width="460">

## Gameplay Framework

##### Gameplay Framework Quick Reference
Short overview of classes for game rules, characters, controllers, user interfaces, etc., that make up the framework for the game.

##### Game Flow Overview
The process of starting the engine and launching a game or play-in-editor session.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Framework/gameflow_lander.webp" width="460">

##### Game Mode and Game State
Overview of the Game Mode and Game State

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Framework/gamemode_lander.webp" width="460">

##### Pawn
The Pawn is the physical representation of a player within the world.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Framework/pawn_lander.webp" width="460">

##### Controller
In the context of a player or AI entity, the Controller is essentially the brain.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Framework/controller_lander.webp" width="460">

##### Camera
The Camera represents the player's point of view; how the player sees the world.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Framework/camera_lander.webp" width="460">

##### User Interfaces & HUDs
Guides and information for artists and programmers creating user interfaces such as menus and HUDs.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Framework/hud_lander.webp" width="460">

## Networking

##### [Обзор Networking](https://gitlab.com/Goonier/unreal-engine-4-rus-by-mishamouse/-/blob/main/Making%20Interactive%20Experiences/Networking/Networking%20Overview.md)
Настройка сетевых игр для мультиплеера.

##### Setting Up Dedicated Servers
How to set up and package a dedicated server for your project.

##### Multiplayer Programming Quick Start
Create a simple multiplayer game in C++.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Networking/QuickStart/Preview.webp" width="460">

##### Actor Replication
The various aspects of replicating Actor objects and their components.

##### Client-Server Model
An overview of the role of the server in multiplayer.

##### Character Movement Component
Detailed explanation of Character Movement Component

##### Online Beacons
Overview of the Online Beacon system

##### Testing Multiplayer
Set up the Unreal Editor for testing multiplayer games.

##### Multiplayer in Blueprints
The various aspects of multiplayer applied to Blueprints.

##### Using Steam Sockets
How to enable the Steam network protocol layer for Unreal 4 projects.

##### Replication Graph
Overview of the Replication Graph feature and Replication Graph Nodes.

##### Travelling in Multiplayer
An overview of how travelling works in multiplayer.

##### Network Profiler
Tool for displaying network traffic and performance information captured at runtime.

##### Performance and Bandwidth Tips
Some tips for optimizing the performance and bandwidth usage of Actor replication.

## Physics

##### APEX
Creation and Import of Destructibles using the NVIDIA APEX Toolset.

##### Collision
All about collision, collision responses, and collision presets.

##### Physics Constraints
Constraining physics objects together and to the world.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Physics/physics_topic.webp" width="460">

##### Damping and Friction
Properties and methods that enable physical objects to resist movement.

##### Physical Materials
Assets applied to physically simulated primitives, directly or via materials, used to configure and control physical properties used by the simulation.

##### Physics Asset Editor
The editor used to set up physics bodies and constraints for physical simulation and collision for Skeletal Meshes.

##### Physics Bodies
The basis for most physics interactions: the Physics Body [BodyInstance].

##### 4.21 Physics Technical Notes
Describes the technical changes made to the Physics Interface in the 4.21 engine release.

##### Physics Sub-Stepping
Explanation of what Physics Sub-Stepping is and when to use it.

##### Walkable Slope
What the Walkable Slope Override is and the situations you may want to use it in.

##### Simple versus Complex Collision
Reference about collision complexity flags and when to use them.

##### Clothing Tool
An overview of Cloth creation using the in-Editor tools with Unreal Engine.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Physics/Cloth/Overview/Clothing_Topic.webp" width="460">

##### Chaos Physics
Chaos Physics is Fornite's lightweight physics solver.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/Physics/ChaosPhysics/chaosphysics-topicimg.webp" width="460">

## AI

##### Behavior Trees
Documents the Behavior Trees asset in Unreal Engine 4 (UE4) and how it can be used to create Artificial Intelligence (AI) for non-player characters in your projects.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/ArtificialIntelligence/BehaviorTrees/BehaviorTree_Landing_Topic.webp" width="460">

##### Navigation System
Describes the different components of the Navigation System available in Unreal Engine.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/ArtificialIntelligence/NavigationSystem/BasicNav_TopicImage.webp" width="460">

##### Environment Query System
Documents the Environment Query System (EQS) and how it can be used to query the environment for data. That data can then be used to provide the AI with data used in the decision making process on how to proceed.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/ArtificialIntelligence/EQS/EQS_Topic_Image.webp" width="460">

##### AI Perception
Documents the AI Perception Component and how it is used to generate awareness for AI.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/ArtificialIntelligence/AIPerception/AI_Perception_Topic.webp" width="460">

##### AI Debugging
Describes the different ways in which you can debug your AI with the AI Debugging Tools.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/ArtificialIntelligence/AIDebugging/Enabled_AI_Debugging.webp" width="460">

## UMG User Interface

##### UMG UI Designer - How To
A How To Guide for creating UI elements with Unreal Motion Graphics UI Designer (UMG).

##### UMG UI Designer Quick Start Guide
Getting started with using Unreal Motion Graphics in Unreal Engine 4.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/UMG/QuickStart/UMGQS_Topic.webp" width="460">

##### UMG UI Designer User Guide
A guide to using Unreal Motion Graphics to create UI elements.

## How-To Guides

##### Setting Up Character Movement
In this How-To guide, you will create a playable character that exhibits different forms of Character Movement.

##### Respawning a Player Character
A how-to guide for respawning player characters in Unreal Engine.

##### Possessing Pawns
A How To Guide for possessing different pawns in Unreal Engine 4.

##### Setting Up a Game Mode
A How To Guide for Setting Up a Game Mode in Unreal Engine 4.

##### Finding Actors
A How To Guide for Finding Actors in your Scenes in Unreal Engine 4.

##### Using the OnHit Event
A How To Guide to for using the OnHit Event in Blueprints or Code.

##### Adding Components to an Actor
A How To Guide for Adding Components to Actors in Unreal Engine 4.
