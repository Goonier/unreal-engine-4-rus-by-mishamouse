# Что нового
Информация о новых функциях каждого релиза Unreal Engine

<img src="https://docs.unrealengine.com/4.27/Images/WhatsNew/Builds/ReleaseNotes/4_25/image_0.webp" width="1200">

Узнайте о новых функциях, доступных с каждым релизом, а также о некоторых передовых функциях, представленных в Beta или Experimental стадиях.

##### Примечания к выпуску
Примечания к выпуску для билдов Unreal Engine.
