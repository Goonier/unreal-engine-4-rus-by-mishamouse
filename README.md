# Unreal Engine 4 Rus by MishaMouse

## Документация Unreal Engine 4

##### [Что нового](https://gitlab.com/Goonier/unreal-engine-4-rus-by-mishamouse/-/blob/main/What's%20New/What's%20New.md)
Информация о новых функциях каждого релиза Unreal Engine

<img src="https://docs.unrealengine.com/4.27/Images/WhatsNew/Builds/ReleaseNotes/4_25/image_0.webp" width="460">

##### Understanding the Basics
Foundational concepts and skills that are essential for any audience to use Unreal for any purpose.

<img src="https://docs.unrealengine.com/4.27/Images/Basics/Actors/Transform/actortransform_topic.webp" width="460">

##### Working with Content
Information on using art created in external applications, importing it into Unreal Engine 4, and setting it up for use in visualization and interactive applications.

<img src="https://docs.unrealengine.com/4.27/Images/WorkingWithContent/content_topic.webp" width="460">

##### Building Virtual Worlds
Information on the tools and techniques for interactive environment and level design.

<img src="https://docs.unrealengine.com/4.27/Images/BuildingWorlds/worlds_topic.webp" width="460">

##### Designing Visuals, Rendering, and Graphics
Rendering subsystem including lighting and shadowing, materials and textures, visual effects, and post processing.

<img src="https://docs.unrealengine.com/4.27/Images/RenderingAndGraphics/RAndG_TopicSmall.webp" width="460">

##### Programming and Scripting
How to use the programming and scripting languages and tools for controlling Unreal engine programmatically at runtime.

<img src="https://docs.unrealengine.com/4.27/Images/ProgrammingAndScripting/ProgrammingAndScripting_TopicImage.webp" width="460">

##### [Making Interactive Experiences](https://gitlab.com/Goonier/unreal-engine-4-rus-by-mishamouse/-/blob/main/Making%20Interactive%20Experiences/Making%20Interactive%20Experiences.md)
How to create gameplay mechanics, behaviors, and conditions that make the virtual world responsive to players carrying out actions over time.

<img src="https://docs.unrealengine.com/4.27/Images/InteractiveExperiences/InteractiveExperiences_TopicImage.webp" width="460">

##### Animating Characters and Objects
Introducing movement in the form of cinematics and character animation.

<img src="https://docs.unrealengine.com/4.27/Images/placeholder_topic.webp" width="460">

##### Working with Media
How to play linear video in your projects, and how to integrate Unreal's rendered frames into virtual production and broadcast scenarios.

<img src="https://docs.unrealengine.com/4.27/Images/WorkingWithMedia/TopicImage_WorkingWithMedia.webp" width="460">

##### Setting Up Your Production Pipeline
Things you should understand and set up in order to get a team of people working efficiently with Unreal.

<img src="https://docs.unrealengine.com/4.27/Images/ProductionPipelines/productionpipelines-topicimg.webp" width="460">

##### Testing and Optimizing Your Content
How to make sure your content does what you expect it to, at the quality and frame rates that you need.

<img src="https://docs.unrealengine.com/4.27/Images/TestingAndOptimization/testingandoptimization-topicimg.webp" width="460">

##### Sharing and Releasing Projects
Information on developing for platforms other than PC.

<img src="https://docs.unrealengine.com/4.27/Images/placeholder_topic.webp" width="460">

##### Samples and Tutorials
Links to various example scenes, sample games, and tutorials.

<img src="https://docs.unrealengine.com/4.27/Images/Resources/resource_depot_topic.webp" width="460">

## Programming and Scripting Reference

##### Unreal Engine C++ API Reference
A complete reference for the Unreal Engine C++ API.

##### Unreal Engine Blueprint API Reference
A complete reference for the Unreal Engine Blueprint API.

##### Unreal Engine Python API Reference
A complete reference for the Unreal Engine Python API.
